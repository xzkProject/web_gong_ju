/**
 * http请求
 * get请求，post请求
 * 上传文件，下载文件
 */
const httpUtils = {
	//header: {
	//    'custom-header': 'hello' //自定义请求头信息
	// },
	requestGet(urlT, params, headers, successCall, failCall) {
		console.log("get开始:" + urlT);
		var requestTask = uni.request({
			url: urlT,
			data: params,
			header: headers,
			method: "GET",
			timeout: 60000,
			dataType: "json",
			withCredentials: true, //跨域请求时是否携带凭证（cookies）
			success(res) {
				console.log("success data:" + res.data);
				console.log("statusCode:" + res.statusCode);
				console.log("header:" + res.header);
				console.log("cookies:" + res.cookies);
				successCall(res);
			},
			fail(res) {
				console.log("fail data:" + res.errMsg);
				failCall(res);
			},
			complete(res) {
				console.log("complete data:" + res.data);
				console.log("complete data:" + res.errMsg);
				console.log("statusCode:" + res.statusCode);
				console.log("header:" + res.header);
				console.log("cookies:" + res.cookies);

			}
		});
	},
	requestPost(urlT, params, headers) {
		var requestTask = uni.request({
			url: urlT,
			data: params,
			header: headers,
			method: "POST",
			timeout: 60000,
			dataType: "json",
			withCredentials: true, //跨域请求时是否携带凭证（cookies）
			success(res) {
				console.log("data:" + res.data);
				console.log("statusCode:" + res.statusCode);
				console.log("header:" + res.header);
				console.log("cookies:" + res.cookies);
			},
			fail(res) {
				console.log("data:" + res.data);
				console.log("statusCode:" + res.statusCode);
				console.log("header:" + res.header);
				console.log("cookies:" + res.cookies);

			},
			complete(res) {
				console.log("data:" + res.data);
				console.log("statusCode:" + res.statusCode);
				console.log("header:" + res.header);
				console.log("cookies:" + res.cookies);

			}
		});
	},
	//todo 如何让将下载的临时文件地址，转为永久的文件呢？
	requestDownload(urlI, headers, filePaths,progressCall,successCall,failCall) {
		var requestDownload = uni.downloadFile({
			url: urlI,
			header: headers,
			timeout: 300000,
			filePath: filePaths,
			success: (downloadFileRes) => {
				console.log("data:" + downloadFileRes.errMsg);
				console.log("statusCode:" + downloadFileRes.statusCode);
				console.log("data:" + downloadFileRes.tempFilePath);
				successCall(downloadFileRes);
			},
			fail(res) {
				console.log("data:" + res.errMsg);
				console.log("statusCode:" + res.statusCode);
				console.log("data:" + res.tempFilePath);
				failCall(res);
			},
			complete(res) {
				console.log("complete sdata:" + res.errMsg);
				console.log("statusCode:" + res.statusCode);
				console.log("data:" + res.tempFilePath);
			}
		});

		requestDownload.onProgressUpdate((res) => {
			console.log('下载进度' + res.progress);
			console.log('已经下载的数据长度' + res.totalBytesWritten);
			console.log('预期需要下载的数据总长度' + res.totalBytesExpectedToWrite);
				progressCall(res);
			// 测试条件，取消下载任务。
			if (res.progress > 50) {
				// requestDownload.abort();
			}
		});
	},
	/**
	 * @param {Object} urlI
	 * @param {Object} fileArr
	 * @param {Object} fileTypeStr
	 * @param {Object} headers
	 * @param {Object} formParams  {"key":"value"}
	 */
	requestUpload(urlI, fileArr, fileTypeStr, headers, formParams) {
		//fileArr
		//name	String	否	multipart 提交时，表单的项目名，默认为 file
		//file	File	否	要上传的文件对象，仅H5（2.6.15+）支持
		//uri	String	是	文件的本地地址
		var requestUpload = uni.uploadFile({
			url: urlI, //仅为示例，非真实的接口地址
			files: fileArr,
			fileType: fileTypeStr,
			name: 'file',
			header: headers,
			timeout: 300000,
			formData: formParams,
			success: (uploadFileRes) => {
				console.log(uploadFileRes.data);
				console.log("data:" + uploadFileRes.data);
				console.log("statusCode:" + uploadFileRes.statusCode);

			},
			fail(res) {
				console.log("data:" + res.data);
				console.log("statusCode:" + res.statusCode);
			},
			complete(res) {
				console.log("data:" + res.data);
				console.log("statusCode:" + res.statusCode);
			}
		});
		//   设置上传进度的文字
		requestUpload.onProgressUpdate((res) => {
			console.log('上传进度' + res.progress);
			console.log('已经上传的数据长度' + res.totalBytesSent);
			console.log('预期需要上传的数据总长度' + res.totalBytesExpectedToSend);

			// 测试条件，取消上传任务。
			if (res.progress > 50) {
				// requestUpload.abort();
			}
		});
	},
}

export default {
	requestGet: httpUtils.requestGet,
	requestPost: httpUtils.requestPost,
	requestDownload: httpUtils.requestDownload,
	requestUpload: httpUtils.requestUpload,
}
