/**
 * socket封装
 * 连接，断开连接，
 * 发送文本消息，发送心跳消息，发送图片消息，发送音频消息，发送视频消息，
 * 接收文本消息，心跳消息，图片消息，音频消息，视频消息
 * 
 在各个小程序平台运行时，网络相关的 API 在使用前需要配置域名白名单。
 只作为工具使用，状态在vue中保存，
 socket连接，旧版本支支持一个，新版本在5个及以上连接数
 */

const socketUtils={
	socketConnect(urls){
		var socketTask =  uni.connectSocket({
		    url: urls,  
		    data() {
		        return {
		            x: '',
		            y: ''
		        };
		    },
		    header: {
		        'content-type': 'application/json'
		    },
		    protocols: ['protocol1'],
		    method: 'GET',
			success(res){
				
			},
			fail(res){
				
			}
		});
		return socketTask;
	}
}