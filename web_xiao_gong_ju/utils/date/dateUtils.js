import dateConstant from './dateConstant.js'
/**
 * 时间戳
 * @param {*} timestamp  时间戳
 */
const timestampToTime = (timestamp) => {
	let date = new Date(timestamp) //时间戳为10位需*1000，时间戳为13位的话不需乘1000
	let Y = date.getFullYear() + '-'
	let M =
		(date.getMonth() + 1 < 10 ?
			'0' + (date.getMonth() + 1) :
			date.getMonth() + 1) + '-'
	let D =
		(date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + ' '
	let h =
		(date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':'
	let m =
		(date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) +
		':'
	let s =
		date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds()
	return Y + M + D + h + m + s
};

const Utils = {
	formatTime: (time) => {
		if (typeof time !== 'number' || time < 0) {
			return time
		}

		var hour = parseInt(time / 3600)
		time = time % 3600
		var minute = parseInt(time / 60)
		time = time % 60
		var second = time

		return ([hour, minute, second]).map(function(n) {
			n = n.toString()
			return n[1] ? n : '0' + n
		}).join(':')
	},
	toTimestamp: (date, num) => {
		var num = parseInt(num);
		if (isNaN(num)) {
			num = 1000;
		}
		return Date.parse(new Date(date)) / num;
	},
	/**var time=new Date(parseInt(1420184730) * 1000).format('yyyy年M月d日');
	 * 月(M)、日(d)、小时(h)、分(m)、秒(s) 毫秒(S)、季度(q) 可以用 1-2 个占位符，
	 * 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
	 * 例子：
	 * (new Date()).format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423
	 * */
	formatDate: (timep, timetype) => {
		if (timep && parseInt(timep) != timep) {
			// 判断时间戳是否是数字，不是数字原数据返回
			return timep;
		} else if (!timep) {
			var timep = Date.parse(new Date()) / 1000;
		} else if (timep.length == 10) {
			var timep = parseInt(timep)
		} else if (timep.length > 10) {
			var timep = timep.substring(0, 10);
		}
		// return '收到'+timep;

		var timetype = timetype || "yyyy-M-d hh:mm:ss";
		Date.prototype.format = function(fmt) { //author: meizz
			var o = {
				"M+": this.getMonth() + 1, //月份
				"d+": this.getDate(), //日
				"h+": this.getHours(), //小时
				"m+": this.getMinutes(), //分
				"s+": this.getSeconds(), //秒
				"q+": Math.floor((this.getMonth() + 3) / 3), //季度
				"S": this.getMilliseconds() //毫秒
			};
			if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp
				.$1.length));
			for (var k in o)
				if (new RegExp("(" + k + ")").test(fmt))
					fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr((
						"" + o[k]).length)));
			return fmt;
		}
		var time = new Date(parseInt(timep) * 1000).format(timetype);
		return time;
	},
	getTime: (num) => {
		var num = parseInt(num);
		if (isNaN(num)) {
			num = 1000;
		}

		return Date.parse(new Date()) / num;
	}
}
const dateUtils = {
	UNITS(unit) {
		if(unit == '年'){
			return 31557600000;
		}else if(unit == '月'){
			return 2629800000;
		}else if(unit == '天'){
			return 86400000;
		}else if(unit == '小时'){
			return 3600000;
		}else if(unit == '分钟'){
			return 60000;
		}else if(unit == '秒'){
			return 1000;
		}
		return 0;
	},
	// 毫秒的单位
	humanize: (milliseconds ) => {
		var humanize = '';
		for (var key in dateConstant.UNITS) {
			if (milliseconds >= dateConstant.UNITS[key]) {
				humanize = Math.floor(milliseconds / dateConstant.UNITS[key]) + key + '前';
				break;
			}
		}
		return humanize || '刚刚';
	},
	format: (dateStr) => {
		var date = this.parse(dateStr)
		var diff = Date.now() - date.getTime();
		if (diff < dateConstant.UNITS['天']) {
			return this.humanize(diff);
		}
		var _format = (number) => {
			return (number < 10 ? ('0' + number) : number);
		};
		return date.getFullYear() + '/' + _format(date.getMonth() + 1) + '/' + _format(date.getDay()) + '-' +
			_format(date.getHours()) + ':' + _format(date.getMinutes());
	},
	parse: (str) => { //将"yyyy-mm-dd HH:MM:ss"格式的字符串，转化为一个Date对象
		var a = str.split(/[^0-9]/);
		return new Date(a[0], a[1] - 1, a[2], a[3], a[4], a[5]);
	},
	parseTimeByType(time,type) {
		//按照格式解析时间，
		//str.substr方法在第一次调用时是按照开始结束位置截取的，但是第二次以后就按照起始位置到字符串结束位置截取并不会按照指定的结束位置截取
		// slice方法能按照指定的开始 结束 位置进行截取。
		var year = "0000";
		var mouth = "00";
		var day = "00";
		var hour = "00";
		var minth = "00";
		var seconds = "00";
		if (type == dateConstant.dateYMD_HMS) {
			year = time.slice(0, 4);
			mouth = time.slice(5, 7);
			day = time.slice(8, 10);
			hour = time.slice(11, 13);
			minth = time.slice(14, 16);
			seconds = time.slice(17, 19);
		}else if (type == dateConstant.dateYMDHMS) {
			year = time.slice(0, 4);
			mouth = time.slice(4, 6);
			day = time.slice(6, 8);
			hour = time.slice(8, 10);
			minth = time.slice(10, 12);
			seconds = time.slice(12, 14);
		}else if (type == dateConstant.dateYMD) {
			year = time.slice(0, 4);
			mouth = time.slice(4,6);
			day = time.slice(6, 8); 
		}else if (type == dateConstant.dateHMS) {
			hour = time.slice(0, 2);
			minth = time.slice(2, 4);
			seconds = time.slice(4, 6); 
		}else if(transformType == dateConstant.dateHMS_mo){
			hour = time.slice(0, 2);
			minth = time.slice(3, 5);
			seconds = time.slice(6, 8); 
		}
		return new Date(year+"-"+mouth+"-"+day+" "+hour+":"+minth+":"+seconds);
	},
	parseTimeStrByType(timeDate,type,transformType) {
		//按照格式将时间转为目标显示类型的字符串，
		var date= this.parseTimeByType(timeDate,type);
		//UTC时间与北京时间相差8小时的时区，注意取值问题
		var _format = (number) => {
			return (number < 10 ? ('0' + number) : number);
		};
		var year =  date.getFullYear();
		var mouth = _format(date.getMonth()+1);
		var day = _format(date.getDate());
		var hour = _format(date.getHours());
		var minth = _format(date.getMinutes());
		var seconds = _format(date.getSeconds());
		console.log("年："+year+",月:"+mouth+",日:"+day);
		 
		if(transformType == dateConstant.dateHMS_mo){
			return hour+":"+minth+":"+seconds;
		} else if (transformType == dateConstant.dateHMS){
			return hour+""+minth+""+seconds;
		}else if (transformType == dateConstant.dateYMD){
			return year+""+mouth+""+day;
		}else if (transformType == dateConstant.dateYMDHMS){
			return year+""+mouth+""+day+""+hour+""+minth+""+seconds;
		}else if (transformType == dateConstant.dateYMD_HMS){
			return year+"-"+mouth+"-"+day+" "+hour+":"+minth+":"+seconds;
		}
		return "未解析的新类型";
	}
}

export default {
	timestampToTime: timestampToTime,
	toTimestamp: Utils.toTimestamp, //日期时间转成时间戳
	getTime: Utils.getTime, //当前时间戳 
	formatDate: Utils.formatDate, //时间戳转日期  
	UNITS: dateUtils.UNITS, //日期时间转成时间戳
	humanize: dateUtils.humanize, //日期时间转成时间戳
	format: dateUtils.format, //日期时间转成时间戳
	parse: dateUtils.parse, //日期时间转成时间戳
	dateConstant: dateConstant,
	parseTimeByType:dateUtils.parseTimeByType,
	parseTimeStrByType:dateUtils.parseTimeStrByType,
}
