
// 时间格式类型，
const dateYMD_HMS="yyyy-mm-dd hh:MM:ss";
const dateYMDHMS="yyyymmddhhMMss";
const dateYMD="yyyymmdd";
const dateHMS="hhMMss";
const dateHMS_mo="hh:MM:ss";


const UNITS ={
	'年':31557600000,
	'月':2629800000,
	'天':86400000,
	'小时':3600000,
	'分钟':60000,
	'秒':1000, 
	};

export default{
	dateYMD_HMS:dateYMD_HMS,
	dateYMDHMS:dateYMDHMS,
	dateYMD:dateYMD,
	dateHMS:dateHMS,
	dateHMS_mo:dateHMS_mo,
	UNITS:UNITS,
}