/**
 * 文件操作页面  ，选择查看文件的属性，完整路径，文件名，文件类型，文件创建日期，文件修改日期，文件内容写入，文件内容读取，
 * 创建文件，创建文件夹，删除文件，删除文件夹，
 * 
 
 一、功能实现核心：FileSystemObject 对象
 
 其实，要在Javascript中实现文件操作功能，主要就是依靠FileSystemobject对象。
 这两天着手进行数据的可视化问题，需要使用javascript进行本地文件的操作，但是使用浏览器访问本地文件一直不太方便。为了在浏览器中能够实现诸如拖拽并上传本地文件这样的功能，我们就不得不求助于特定浏览器所提供的各种技术了。比如对于 IE，我们需要通过 ActiveX 控件来获取对本地文件的访问能力，而对于 Firefox，同样也要借助插件开发。由于不同浏览器的技术实现不尽相同，为了让程序能够支持多浏览器，我们的程序就会变得十分复杂而难于维护。
 经过实践尝试，下面介绍两种可行的方式：
 
 一、使用FileSystemObject
  二、使用javascript 的API
  并非标准的api，所以无法使用，需要插件，并且没有原始的文件名可以使用，很难搞。
  
 */
const fileUtil = {
	getFilePath() {
		uni.chooseFile({
			count: 1,
			type: "all",
			success(tempFilePaths) {
				var arrFiles = tempFilePaths.tempFiles;
				console.log("选择的结果:" + arrFiles);
				for (var fileIn in arrFiles) {//这里定义的是集合中的key
					console.log("选择的结果:" + arrFiles[fileIn].path);
					console.log("选择的结果:" + arrFiles[fileIn].lastModified);
					console.log("选择的结果:" + arrFiles[fileIn].lastModifiedDate);
					console.log("选择的结果:" + arrFiles[fileIn].name);
					console.log("选择的结果:" + arrFiles[fileIn].size);
					console.log("选择的结果:" + arrFiles[fileIn].type);
				}
			},
			fail(fa) {
				console.log("选择的失败:"+fa);
			},
			complete(cc) {
				console.log("选择完成:"+cc);
				var arrFiles = cc.tempFiles;
				console.log("选择的结果:" + arrFiles);
				for (var fileIn in arrFiles) {//这里定义的是集合中的key
					console.log("选择的结果:" + arrFiles[fileIn].path);
					console.log("选择的结果:" + arrFiles[fileIn].lastModified);
					console.log("选择的结果:" + arrFiles[fileIn].lastModifiedDate);
					console.log("选择的结果:" + arrFiles[fileIn].name);
					console.log("选择的结果:" + arrFiles[fileIn].size);
					console.log("选择的结果:" + arrFiles[fileIn].type);
				}
			}
		});
	},
	getFileName() {

	},
	getFileType() {

	},
	getFileCreateTime() {

	},
	getFileUpdateTime() {

	},
	writeFileByte() {
		// var fso, test;  
		// fso = new ActiveXObject("Scripting.FileSystemObject");  
		// // 创建新文件  
		// test = fso.CreateTextFile("c:\\1.txt", true);  
		// // 填写数据，并增加换行符  
		// test.WriteLine("1,2,3,4") ;   
		// // 填写一行，不带换行符  
		// test.Write ("hello world");  

		//  //关闭文件
		//  f.close();
	},
	readFileByte() {

	},
	createFile() {
		// var fso = new ActiveXObject("Scripting.FileSystemObject");  
		// var f = fso.createtextfile("c:\\1.txt",true");

		// var ForWriting= 2;  
		// fso = new ActiveXObject("Scripting.FileSystemObject");  
		// test = fso.OpenTextFile("c:\\1.txt", ForWriting, true);

		// var fso, f, test;  
		// var ForWriting = 2;  
		// fso = new ActiveXObject("Scripting.FileSystemObject");  
		// fso.CreateTextFile ("c:\\1.txt");  
		// f = fso.GetFile("c:\\1.txt");  
		// test = f1.OpenAsTextStream(ForWriting, true);

	},
	createFileDir() {

	},
	deleteFile() {

	},
	deleteFileDir() {

	}

}

export default {
	getFilePath: fileUtil.getFilePath,
	getFileName: fileUtil.getFileName,
	getFileType: fileUtil.getFileType,
	getFileCreateTime: fileUtil.getFileCreateTime,
	getFileUpdateTime: fileUtil.getFileUpdateTime,
	writeFileByte: fileUtil.writeFileByte,
	readFileByte: fileUtil.readFileByte,
}
